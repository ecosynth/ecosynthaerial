import os
import sys
import StringIO


lib_path = os.path.abspath('../')
sys.path.append(lib_path)

import ecosynth.acquire as acq
import ecosynth.postprocess as pp

def georef_test(testfiles):
    msl_float = 50.6

    out_filepath = os.path.join(testfiles, 'other/serc511.out')
    out_file = open(out_filepath, mode='r')

    log_filepath = os.path.join(testfiles, 'other/serc511.log.txt')
    log_file = open(log_filepath, mode='r')

    gps_file_buf = StringIO.StringIO()
    acq.utilities.telemetry_to_gps_txt(
        log_file, gps_file_buf, msl_float)
    gps_file_buf.seek(0)

    logger_file = open('debug.txt', 'w')
    logger_file.write("Debugging File\n")

    xyzrgb_array_georef, parameters = pp.transforms.georef_telemetry(
        out_file, gps_file_buf, msl_float, logger=logger_file)

    print(xyzrgb_array_georef)
    print(parameters)

    out_file.close()
    log_file.close()
    logger_file.close()


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    georef_test(testfiles)


if __name__ == "__main__":
    main()
