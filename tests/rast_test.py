import os
import sys
import StringIO
import time

import numpy as np

lib_path = os.path.abspath('../')
sys.path.append(lib_path)

import ecosynth.acquire as acq
import ecosynth.postprocess as pp
from ecosynth.postprocess import CloudRasterizer

def rast_test(testfiles):
    start = time.time()


    msl_float = 50.6

    out_filepath = os.path.join(testfiles, 'test_set_serc/serc511.out')
    out_file = open(out_filepath, mode='r')

    out_tuple = pp.io.load_out(out_file)
    print "cam_array_fixed shape", out_tuple.cam_array_fixed.shape
    print "cam_array shape", out_tuple.cam_array.shape

    log_filepath = os.path.join(testfiles, 'test_set_serc/serc511.log.txt')
    log_file = open(log_filepath, mode='r')

    gps_array = acq.utilities.telemetry_to_gps_positions(
        log_file, msl_float)

    logger_file = open('debug.txt', 'w')
    logger_file.write("Debugging File\n")

    xyzrgb_array_georef, parameters = pp.transforms.georef_telemetry(
        out_tuple, gps_array, msl_float, logger=logger_file)

    """
    array_size = 20000

    xyz_array_georef = np.random.rand(array_size, 3)
    xyz_array_georef *= 100

    rgb_array = np.ones([array_size, 3])
    rgb_array[:, 0] *= 0
    rgb_array[:, 1] *= 0
    rgb_array[:, 2] *= 255

    xyzrgb_array_georef = np.hstack((xyz_array_georef, rgb_array))
    #print xyzrgb_array_georef


    """
    c = CloudRasterizer.CloudRasterizer(
        xyzrgb_array_georef, resolution=1.0)

    c.filter_noise_z()

    png_file = open('img.png', 'w')

    color_raster = c.get_color_raster()
    c.plot_raster_colors(color_raster, title="Color Raster", png_file=png_file)


    max_height_raster = c.get_max_height_raster()
    c.plot_raster_heatmap(max_height_raster)  #, png_file=png_file)
    """
    hr_raster = c.get_height_range_raster()
    c.plot_raster_heatmap(hr_raster, title="Height Range Raster")

    q95_raster = c.get_Q95_elevation_raster()
    c.plot_raster_heatmap(q95_raster, title="Q95 Raster")

    color_raster = c.get_color_raster()
    print color_raster.shape
    c.plot_raster_colors(color_raster, title="Color Raster")

    std_raster = c.get_std_raster()
    c.plot_raster_heatmap(std_raster, title="Standard Deviation Raster")

    point_density_raster = c.get_point_density_raster()
    c.plot_raster_heatmap(point_density_raster, title="Point Density Raster")

    median_height_raster = c.get_median_height_raster()
    c.plot_raster_heatmap(median_height_raster, title="Median Height Raster")

    mean_height_raster = c.get_mean_height_raster()
    c.plot_raster_heatmap(mean_height_raster, title="Mean Height Raster")

    max_elevation_raster = c.get_max_elevation_raster()
    c.plot_raster_heatmap(max_elevation_raster, title="Max Height Raster")

    cv_height_raster = c.get_cv_height_raster()
    c.plot_raster_heatmap(cv_height_raster, title="CV Height Raster")


    print c.get_shape()

    out_file.close()
    log_file.close()
    logger_file.close()

    """


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    rast_test(testfiles)


if __name__ == "__main__":
    main()
