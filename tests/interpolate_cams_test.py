import os
import sys

import ecosynth.generate.utilities as gen

lib_path = os.path.abspath('../')
sys.path.append(lib_path)


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')

    log_filepath = os.path.join(testfiles, 'test_set_serc/serc511.log.txt')
    log_file = open(log_filepath, mode='r')
    msl_float = 5.6
    cam_filepath = os.path.join(testfiles, "test_set_serc/images/")
    cam_xyz_file = open("cams_xyz.txt", 'w')

    gen.interpolate_gps_positions_for_cams(
        log_file, msl_float, cam_filepath, cam_xyz_file)

    log_file.close()
    cam_xyz_file.close()

if __name__ == "__main__":
    main()
