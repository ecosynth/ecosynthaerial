    import os
import sys
import StringIO


lib_path = os.path.abspath('../')
sys.path.append(lib_path)

import ecosynth.acquire as acq
import ecosynth.postprocess as pp

def postprocess_test(testfiles):
    msl_float = 50.6

    out_filepath = os.path.join(testfiles, 'other/serc511.out')
    out_file = open(out_filepath, mode='r')

    log_filepath = os.path.join(testfiles, 'other/serc511.log.txt')
    log_file = open(log_filepath, mode='r')

    gps_file_buf = StringIO.StringIO()

    logger_file = open('debug.txt', 'w')
    logger_file.write("Debugging File\n")

    ply_file = open('new.ply', 'w')

    pp.postprocess.run(
        out_file, log_file, msl_float, ply_file=ply_file, gps_file=gps_file_buf, logger_file=logger_file)

    out_file.close()
    log_file.close()
    logger_file.close()
    ply_file.close()


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    postprocess_test(testfiles)


if __name__ == "__main__":
    main()
