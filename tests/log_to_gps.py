import os
import sys

lib_path = os.path.abspath('../')
sys.path.append(lib_path)
import ecosynth


def ardu_test(testfiles):
    log_file = os.path.join(testfiles, 'other/herbert1221.log.txt')
    log_file = open(log_file, mode='r')
    gps_file = open('gps_out.txt', mode='w')
    msl_float = 50.6
    ecosynth.acquire.utilities.telemetry_to_gps_txt(
        log_file, gps_file, msl_float)
    log_file.close()
    gps_file.close()


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    ardu_test(testfiles)
    # mikro_test(testfiles)

if __name__ == "__main__":
    main()
