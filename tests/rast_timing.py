import os
import sys
import StringIO
import time

import numpy as np

lib_path = os.path.abspath('../')
sys.path.append(lib_path)

import ecosynth.acquire as acq
import ecosynth.postprocess as pp
from ecosynth.postprocess import CloudVoxelizer

def voxel_timing(testfiles):
    """
    msl_float = 50.6

    out_filepath = os.path.join(testfiles, '5 bundle out/serc511.out')
    out_file = open(out_filepath, mode='r')

    out_tuple = pp.io.load_out(out_file)

    print out_tuple

    log_filepath = os.path.join(testfiles, '1 log/serc511.log.txt')
    log_file = open(log_filepath, mode='r')

    gps_array = acq.utilities.telemetry_to_gps_positions(
        log_file, msl_float)

    logger_file = open('debug.txt', 'w')
    logger_file.write("Debugging File\n")

    xyzrgb_array_georef, parameters = pp.transforms.georef_telemetry(
        out_tuple, gps_array, msl_float, logger=logger_file)

    """

    times = []

    for i in range(1, 20, 4):
        start = time.time()

        array_size = 2**i
        print "Array Size", array_size

        xyz_array_georef = np.random.rand(array_size, 3)
        xyz_array_georef *= 100

        rgb_array = np.ones([array_size, 3]) * 150

        xyzrgb_array_georef = np.hstack((xyz_array_georef, rgb_array))
        #print xyzrgb_array_georef

        end_load_files = time.time()
        print "Load Time:", end_load_files - start

        c = CloudVoxelizer.CloudVoxelizer(
            xyzrgb_array_georef, resolution=1)
        end_rasterization = time.time()
        print "Voxelization Time:", end_rasterization - end_load_files

        c.filter_noise_z()
        end_filter = time.time()
        print "Noise Filter Time:", end_filter - end_rasterization

        c.get_height_range_raster()
        end_get_hr = time.time()
        print "Get HR Raster Time:", end_get_hr - end_filter

        times.append((end_get_hr - start))

        #times.append([(end_load_files - start), (
        #    end_rasterization - end_load_files),
        #    (end_filter - end_rasterization), (end_get_hr - end_filter)])

    """
    out_file.close()
    log_file.close()
    logger_file.close()
    """

    print times


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    voxel_timing(testfiles)


if __name__ == "__main__":
    main()
