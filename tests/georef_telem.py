import os
import sys
import StringIO

import numpy as np

lib_path = os.path.abspath('../')
sys.path.append(lib_path)

import ecosynth.acquire as acq
import ecosynth.postprocess as pp


def georef_test(testfiles):
    msl_float = 50.6

    out_filepath = os.path.join(testfiles, 'test_set_serc/serc511.out')
    out_file = open(out_filepath, mode='r')

    log_filepath = os.path.join(testfiles, 'test_set_serc/serc511.log.txt')
    log_file = open(log_filepath, mode='r')

    gps_file_buf = StringIO.StringIO()
    gps_array = acq.utilities.telemetry_to_gps_positions(
        log_file, msl_float, gps_file=gps_file_buf)

    out_tuple = pp.io.load_out(out_file)

    xyzrgb_array_georef, parameters = pp.transforms.georef_telemetry(
        out_tuple, gps_array, msl_float)

    transformed_xyz_array = pp.transforms.apply_helmert_wo_Tx_Ty(
        parameters, out_tuple.xyzrgbCXY_array[:, 0:3])

    xyzrgbCXY_array = np.hstack((
        transformed_xyz_array, out_tuple.xyzrgbCXY_array[:, 3:]))

    print(xyzrgb_array_georef)
    print(parameters)
    print "without Tx, Ty"
    print(xyzrgbCXY_array)

    ecobrowser_ply_file = open("ecobrowser_ready.ply", 'w')
    pp.io.save_ecobrowser_ply(ecobrowser_ply_file, xyzrgbCXY_array)

    out_file.close()
    log_file.close()
    ecobrowser_ply_file.close()


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    georef_test(testfiles)


if __name__ == "__main__":
    main()
