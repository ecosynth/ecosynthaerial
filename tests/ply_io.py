import os
import sys

lib_path = os.path.abspath('../')
sys.path.append(lib_path)
import ecosynth


def ply_test(testfiles):
    ply_filepath = os.path.join(testfiles, 'other/points510.ply')
    ply_file = open(ply_filepath, mode='r')

    ply = ecosynth.postprocess.io.load_ply(ply_file)

    print "-- header --"
    print ply.header
    print "\npoints_total:", ply.points_total
    print "\n-- xyzrgb_array --"
    print ply.xyzrgb_array

    ply_file.close()

    new_ply_file = open('newply.ply', mode='w')
    ecosynth.postprocess.io.save_ply(new_ply_file, ply)
    new_ply_file.close()


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    ply_test(testfiles)


if __name__ == "__main__":
    main()
