import os
import sys

lib_path = os.path.abspath('../')
sys.path.append(lib_path)
import ecosynth


def out_test(testfiles):
    out_filepath = os.path.join(testfiles, 'test_set_serc/serc511.out')
    out_file = open(out_filepath, mode='r')

    out = ecosynth.postprocess.io.load_out(out_file)

    print "\ncameras_total:", out.cameras_total
    print "\ncameras_used:", out.cameras_used
    print "-- cam_array --"
    print out.cam_array
    print "-- cam_array_fixed --"
    print out.cam_array_fixed
    print "\npoints_total:", out.points_total
    print "\n-- xyzrgb_array --"
    print out.xyzrgb_array
    print "\n-- xyzrgbCXY_array --"
    print out.xyzrgbCXY_array

    print "cam array size", out.cam_array.size
    print "cam array fixed size", out.cam_array_fixed.size

    out_file.close()


def main():
    testfiles = os.path.join(os.getcwd(), 'files/')
    out_test(testfiles)


if __name__ == "__main__":
    main()
