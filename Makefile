
help:
	@echo "clean        removes pyc and py~ files"
	@echo "deps         installs dependencies using pip"

clean:
	find . -name '*.pyc' -delete
	find . -name '*.py~' -delete

deps:
	pip install -r requirements.txt
