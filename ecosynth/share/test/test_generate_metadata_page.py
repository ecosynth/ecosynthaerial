import unittest
from ecosynth.share import utilities

# Here's our "unit tests".
class MetadataTests(unittest.TestCase):

    def setUp(self):
        pass

    def testOne(self):
        # Setup
        synth_id = 3
        ov_author = "Will"
        ov_organization = "Ecosynth"
        ov_doi = "doi:10.5072/FK2"
        acq_site = "SERC"
        acq_lat = "12.3"
        acq_lon = "45.6"
        acq_date = "12/03/2014"
        acq_time = "12:00 PM"
        gen_cv_type = "Ecosynther"
        gen_cv_version = "0.4"
        notes = "Notes!"
        img_file = None
        ply_file = None
        out_file = None
        ecobrowser_ply_file = None

        # Run
        metadata_page = utilities.generate_metadata_page(
            synth_id,
            ov_author,
            ov_organization,
            ov_doi,
            acq_site,
            acq_lat,
            acq_lon,
            acq_date,
            acq_time,
            gen_cv_type,
            gen_cv_version,
            notes,
            img_file,
            ply_file,
            out_file,
            ecobrowser_ply_file)

        #print metadata_page
        f = open("metadata.html", "w")
        f.write(metadata_page)
        f.close()


def main():
    unittest.main()

if __name__ == '__main__':
    main()
