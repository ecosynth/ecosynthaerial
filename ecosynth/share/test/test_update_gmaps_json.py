import unittest
from ecosynth.share import utilities


class MetadataTests(unittest.TestCase):

    def setUp(self):
        pass

    def testOne(self):
        # Setup
        sftp_ip = ''
        sftp_user = ''
        sftp_pw = ''
        synth_id = 5
        acq_lat = -75.2
        acq_lon = 23.0
        acq_site = "SERC"
        ov_author = "will"

        # Run
        utilities.update_gmaps_json(
            sftp_ip,
            sftp_user,
            sftp_pw,
            synth_id,
            acq_lat,
            acq_lon,
            acq_site,
            ov_author
            )


def main():
    unittest.main()

if __name__ == '__main__':
    main()
