import unittest
from ecosynth.share import utilities


class MetadataTests(unittest.TestCase):

    def setUp(self):
        pass

    def testOne(self):
        # Setup
        sftp_ip = ''
        sftp_user = ''
        sftp_pw = ''
        synth_id = 5
        metadata_page = 'This is " the metadata page'

        image_file = open('img.zip', 'w')
        image_file.write('this is the zip file')
        image_file.close()
        image_file = open('img.zip', 'r')

        ply_file = open('sparse.ply', 'w')
        ply_file.write('this is the ply file')
        ply_file.close()
        ply_file = open('sparse.ply', 'r')

        out_file = open('bundle.out', 'w')
        out_file.write('this is the out file')
        out_file.close()
        out_file = open('bundle.out', 'r')

        # Run
        utilities.upload_data(
            sftp_ip,
            sftp_user,
            sftp_pw,
            synth_id,
            metadata_page,
            image_file,
            ply_file,
            out_file
            )


def main():
    unittest.main()

if __name__ == '__main__':
    main()
