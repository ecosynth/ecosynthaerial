import unittest
from ecosynth.share import utilities


class MetadataTests(unittest.TestCase):

    def setUp(self):
        pass

    def testOne(self):
        # Setup
        sftp_ip = ''
        sftp_user = ''
        sftp_pw = ''

        # Run
        synth_id = utilities.verify_sftp(
            sftp_ip,
            sftp_user,
            sftp_pw
            )

        print "Synth ID:", synth_id


def main():
    unittest.main()

if __name__ == '__main__':
    main()
