<?php
    $file = file_get_contents('./map_objects.txt');
    $rows = explode("\n", $file);

    $return_str = "";
    $return_str .= "eqfeed_callback( { \"synthsets\": [";

    $return_str .= $rows[0];  

    for($i = 1; $i < count($rows) - 1; $i++)
    {  
        $return_str .= ", ";  
        $return_str .= $rows[$i];  
    }  

	$return_str .= "] } )";

    echo $return_str;
?>
