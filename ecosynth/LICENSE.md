Ecosynth License
----------------

Copyright (c) 2014 Ecosynth All rights reserved.

The Ecosynth Library is licensed under a Creative Commons Attribution 4.0 International License.