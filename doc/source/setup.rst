=====
Setup
=====

Installing Ecosynth with PIP
============================

1. Install PIP (Python's package manager) if it is not already installed on your computer:

	``easy_install pip``

2. Install the Ecosynth library:

	``pip install ecosynth``


Launching the GUI
=================

From the directory you intend to work in:

	``python -m ecosynth``


Troubleshooting
===============

**Linux and Unix users:**

Make sure your PYTHONPATH environment variable is set correctly. Also, executing certain commands using the 'sudo' command may be necessary at times.


**Windows Users:**

If you are having trouble launching the GUI, make sure to run your command prompt as an adminstrator, which you can do by right-clicking your command prompt and selecting "run as administrator"

We also recommend installing a Scientific Computing for Python distribution such as the Anaconda package at http://continuum.io/downloads.  It can be especially helpful for Windows users get around difficult to debug issues with compiling Numpy and Scipy from source.

For Errno 10013:

First, add Python interpreters to Firewall's allowed applications. Second, remove process that's blocking Port 8081:

1. Go to: Window Task Manager --> Performance --> Resource Monitor --> Network
2. Open: "Listening Ports"
3. Find: Port 8081
4. Record PID (process ID)
5. Go back to "Services" in Windows Task Manager
6. Find service with recorded PID
7. Right click on that PID and click "go to process"
8. Right click on highlighted process
9. Click "End Process Tree"
