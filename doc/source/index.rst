.. Ecosynth documentation master file, created by
   sphinx-quickstart on Tue Jun  3 12:50:00 2014.

======================
EcosynthAerial Library
======================


About
=====
The EcosynthAerial library contains functions to aid users in processing data collected with hobbyist-grade UAVs.

The Ecosynth Lab develops tools used to map and measure vegetation in three dimensions.


Get Started
===========

The simplest way to get started is to execute the following instructions from the command line::

	easy_install pip       # Install Python Package Manager
	pip install ecosynth   # Install Ecosynth Library
	python -m ecosynth     # Launch GUI


Table of Contents
=================


.. toctree::
   :maxdepth: 3

   setup
   reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

