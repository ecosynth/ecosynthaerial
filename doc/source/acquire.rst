Data Acquisition (Stage 1)
==========================

The Data Acquisiton module contains tools to help users through the process of flying a UAV, as well as organizing and filtering the pre-requisite data necessary to generate a point cloud

.. automodule:: acquire.utilities
   :members:


.. End of file