Ecosynth Aerial Tools
=====================

## About

This application provides a complete suite of tools for processing of Ecosynth 3D vegetation scans generated using computer vision.  

The tools allow the user to georeference point clouds based on copter telemetry data and produce map-based datasets, including digitial surface models (DSMs) of canopy and surface elevation, canopy height models (CHMs) showing canopy height normalized to the understory terrain when a digital terrain model (DTM) is provided, and to produce maps of point cloud density, point cloud canopy penetration, and basic level orthoimage mosaics.  

## Requirements

The toolset is written in Python 2.7 and provides a GUI that loads in the users browser based on standard libraries.

To see a full list of requirements, check the requirements.txt file.

## Installation via Download

1. Download and open terminal in base directory of library
2. `sudo python setup.py install`

## Installation via PIP

	easy_install pip
	pip install ecosynth

## Launch GUI

	python -m ecosynth

## Documentation

Currently [Hosted Here](http://wiki.ecosynth.org/index.php?title=Data_Processing)

## License

The Ecosynth Aerial Pipeline Application is licensed under a [Creative Commons Attribution 4.0 International License][id].

[id]: http://creativecommons.org/licenses/by/4.0/ "CC BY 4.0"

# Maintenance on PyPI

## To Push a New Version (Using '0.1.1' as example of new version)

1. From top level directory (`cd ecosynthaerial`)

2. Update 'version' in setup.py to '0.1.1' (and add any new package dependencies to 'install_requires')

3. Create new branch in repository: `git checkout -b 0.1.1`

4. Push new branch to remote repository: `git push origin 0.1.1`

5. Upload new version to PyPI: `python setup.py sdist upload` (enter any necessary account info)  When a new version is added, it becomes the primary downloadable version on PyPI.

6. Return to master branch for further development: `git checkout master`

## To Push a Bug Fix

If you want to keep the version the same, but push a bugfix, you have to manually delete the current version through the PyPI website and then repush the new code with `python setup.py sdist upload`.