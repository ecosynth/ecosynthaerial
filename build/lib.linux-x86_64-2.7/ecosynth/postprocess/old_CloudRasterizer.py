"""
CloudRasterizer
===============
"""

__all__ = ['CloudRasterizer']

import numpy as np

from . import CloudVoxelizer


class CloudRasterizer(CloudVoxelizer.CloudVoxelizer):
    """
    CloudRasterizer Class for processing Point Cloud and enabling 2D analysis

    :param np.array xyzrgb_array:
    :param int resolution: (Optional)
    :param list aoi: (Optional) [xMin, xMax, yMin, yMax]

    :var int resolution: grid size
    :var np.array cloud_array: points in given AOI
    :var float xMin:
    :var float yMin:
    :var np.array raster_grid: 2D grid of arrays filled with XYZRGB points

    | **Notes**
    | here

    | **Example**
    | here

    """
    def __init__(self, xyzrgb_array, resolution=1, aoi=None):
        super(CloudVoxelizer, self).__init__(xyzrgb_array, resolution=resolution, aoi=aoi)
        if not aoi:
            aoi = []
            aoi.append(np.amin(xyzrgb_array[:, 0]))
            aoi.append(np.amax(xyzrgb_array[:, 0]))
            aoi.append(np.amin(xyzrgb_array[:, 1]))
            aoi.append(np.amax(xyzrgb_array[:, 1]))

        self.resolution = resolution
        self.cloud_array = self.subsetCloud(xyzrgb_array, aoi)
        self.xMin = aoi[0]
        self.yMin = aoi[2]
        self.raster_grid = self.cloudToGrid(self.cloud_array, resolution)

    def subsetCloud(self, xyzrgb_array, aoi=None):
        """
        (NOTE: Reconcile similarities with getGridSubset)
        Filters points outside given Area of Interest

        :param np.array xyzrgb_array:
        :param list aoi: (Optional) [xMin, xMax, yMin, yMax]

        :return: xyzrgb_array_filtered
        :rtype: np.array
        """
        if not aoi:
            return xyzrgb_array
        else:
            subset = []
            for point in xyzrgb_array:
                if ((aoi[0] <= point[0]) and (aoi[1] >= point[0])):
                    if ((aoi[2] <= point[1])and (aoi[3] >= point[1])):
                        subset.append(point)

            return np.array(subset)

    def getAOI(self):
        """
        Searches own point_cloud and returns boundary points

        :return: aoi
        :rtype: list [xMin, xMax, yMin, yMax]
        """
        aoi = []
        aoi.append(np.amin(self.cloud_array[:, 0]))
        aoi.append(np.amax(self.cloud_array[:, 0]))
        aoi.append(np.amin(self.cloud_array[:, 1]))
        aoi.append(np.amax(self.cloud_array[:, 1]))

    def cloudToGrid(self, xyzrgb_array, resolution):
        """
        Sorts points into an XY grid and returns grid

        :param np.array xyzrgb_array:
        :param int resolution:

        :return: grid
        :rtype: np.array
        """
        Xmin = np.amin(xyzrgb_array[:, 0])
        Xmax = np.amax(xyzrgb_array[:, 0])
        Ymin = np.amin(xyzrgb_array[:, 1])
        Ymax = np.amax(xyzrgb_array[:, 1])

        # Build empty grid
        grid = []
        buff = int(1/resolution)

        if buff < 1:
            buff = 1

        for x in range(buff + int(int(Xmax - Xmin)/resolution)):
            grid.append([])
            for y in range(buff + int(int(Ymax - Ymin)/resolution)):
                grid[x].append([])

        # Fill Grid
        for i in range(len(xyzrgb_array)):
            x = int((xyzrgb_array[i][0] - Xmin)/resolution)
            y = int((xyzrgb_array[i][1] - Ymin)/resolution)

            grid[x][y].append(xyzrgb_array[i])

        return np.array(grid)

    def get2D(self):
        """
        Returns copy of XY Grid

        :return: raster_grid
        :rtype: np.array
        """
        return np.copy(self.raster_grid)

    def filterNoise():
        """
        Returns a CloudRasterizer object that has been filtered for noise

        :return: CRObject
        :rtype: CloudRasterizer
        """
        # Get Area of Map

        # Build Empty Grid

        # Fill Grid

        # zScoreFilter

        pass

    #It will draw a rectangle using the coordinates given
    def getGridSubset(self, xMin, xMax, yMin, yMax):
        """
        (NOTE: Reconcile similarities with getGridSubset)
        (NOTE: Currelty indexes based on grid rather than GPS coordinates

        Returns subset of XY Grid

        :param int xMin:
        :param int xMax:
        :param int yMin:
        :param int yMax:

        :return: raster_grid_subset
        :rtype: np.array
        """
        xMinScaled = int(xMin / self.resolution)
        xMaxScaled = int(xMax / self.resolution)
        yMinScaled = int(yMin / self.resolution)
        yMaxScaled = int(yMax / self.resolution)

        raster_grid_subset = np.copy(
            self.raster_grid[xMinScaled:xMaxScaled, yMinScaled:yMaxScaled])
        return raster_grid_subset

    def getHoriz(self, layer):
        """
        Unimplemented

        :param one two:

        :return:
        :rtype:
        """
        #return np.copy(self.raster_grid)
        pass

    def getVert(self, layer):
        """
        Unimplemented

        :param one two:

        :return:
        :rtype:
        """
        #return np.copy(self.raster_grid)
        pass

    def filterHeight(self, height, DTM):
        pass

    def mapOverlap(self, CRObject):
        pass
