"""
CloudVoxelizer
==============
"""

__all__ = ['CloudVoxelizer']

import numpy as np


class CloudVoxelizer(object):
    """
    CloudVoxelizer Class for processing Point Cloud and enabling 3D analysis

    :var int resolution: grid size
    :var np.array cloud_array: points in given AOI
    :var float xMin:
    :var float yMin:
    :var float zMin:
    :var np.array voxel_grid: 3D grid of arrays filled with XYZRGB points

    | **Notes**
    | here

    | **Example**
    | here

    """
    def __init__(self, xyzrgb_array, resolution=1, aoi=None):
        """
        :param np.array xyzrgb_array: XYZRGB numpy array
        :param int resolution:
        :pararm list aoi: [xMin, xMax, yMin, yMax, zMin, zMax]
        """
        if not aoi:
            aoi = []
            aoi.append(np.amin(xyzrgb_array[:, 0]))
            aoi.append(np.amax(xyzrgb_array[:, 0]))
            aoi.append(np.amin(xyzrgb_array[:, 1]))
            aoi.append(np.amax(xyzrgb_array[:, 1]))
            aoi.append(np.amin(xyzrgb_array[:, 2]))
            aoi.append(np.amax(xyzrgb_array[:, 2]))

        self.resolution = resolution
        self.cloud_array = self.subsetCloud(xyzrgb_array, aoi)
        self.xMin = aoi[0]
        self.yMin = aoi[2]
        self.zMin = aoi[4]
        self.voxel_grid = self.cloudToGrid(self.cloud_array, resolution)

    def subsetCloud(self, xyzrgb_array, aoi=None):
        """
        Filters points outside given Area of Interest

        :param np.array xyzrgb_array:
        :param list aoi: (Optional) [xMin, xMax, yMin, yMax, zMin, zMax]

        :return: xyzrgb_array_filtered
        :rtype: np.array
        """
        if not aoi:
            return xyzrgb_array
        else:
            subset = []
            for point in xyzrgb_array:
                if ((aoi[0] <= point[0]) and (aoi[1] >= point[0])):
                    if ((aoi[2] <= point[1]) and (aoi[3] >= point[1])):
                        if ((aoi[4] <= point[2]) and (aoi[5] >= point[2])):
                            subset.append(point)

            return np.array(subset)

    def getAOI(self):
        """
        Searches own point_cloud and returns boundary points

        :return: aoi
        :rtype: list [xMin, xMax, yMin, yMax, zMin, zMax]
        """
        aoi = []
        aoi.append(np.amin(self.cloud_array[:, 0]))
        aoi.append(np.amax(self.cloud_array[:, 0]))
        aoi.append(np.amin(self.cloud_array[:, 1]))
        aoi.append(np.amax(self.cloud_array[:, 1]))
        aoi.append(np.amin(self.cloud_array[:, 1]))
        aoi.append(np.amax(self.cloud_array[:, 1]))

    def cloudToGrid(self, xyzrgb_array, resolution):
        """
        Sorts points into an XY grid and returns grid

        :param np.array xyzrgb_array:
        :param int resolution:

        :return: grid
        :rtype: np.array
        """
        Xmin = np.amin(xyzrgb_array[:, 0])
        Xmax = np.amax(xyzrgb_array[:, 0])
        Ymin = np.amin(xyzrgb_array[:, 1])
        Ymax = np.amax(xyzrgb_array[:, 1])
        Zmin = np.amin(xyzrgb_array[:, 2])
        Zmax = np.amax(xyzrgb_array[:, 2])

        # Build empty grid
        grid = []
        buff = int(1/resolution)

        if buff < 1:
            buff = 1

        for x in range(buff + int(int(Xmax - Xmin)/resolution)):
            grid.append([])
            for y in range(buff + int(int(Ymax - Ymin)/resolution)):
                grid[x].append([])
                for z in range(buff + int(int(Zmax - Zmin)/resolution)):
                    grid[x][y].append([])

        # Fill Grid
        for i in range(len(xyzrgb_array)):
            x = int((xyzrgb_array[i][0] - Xmin)/resolution)
            y = int((xyzrgb_array[i][1] - Ymin)/resolution)
            z = int((xyzrgb_array[i][2] - Zmin)/resolution)

            grid[x][y][z].append(xyzrgb_array[i])

        return np.array(grid)

    def get2D(self):
        """
        Returns an XY Grid

        :return: raster_grid
        :rtype: np.array
        """
        newRaster = []
        #takes each y and flattens out the z's
        for x in range(len(self.voxel_grid)):
            newRaster.append([])
            for y in range(len(self.voxel_grid[0])):
                newRaster[x].append([])

                newRaster[x][y] = [
                    item for sublist in self.voxel_grid[x][y] for item in sublist]

        newRaster = np.array(newRaster)
        return newRaster

    def filterHeight(self, height, DTM):
        """
        (Unimplemented)

        Removes all points below the specified height, assuming that the
        DTM raster is of the same XY area as the Map and they are using
        the same coordinate system. Any area where the DTM says there is
        no data is ignored and the points over that area filtered out

        :param workableDTM DTM:
        :param float height: distance between ground and point

        :return: filteredCR
        :rtype: CloudRasterizer
        """
        #Throws an error if the DTM and map resolutions do not match
        #This should be added in elsewhere
        if DTM.resolution != self.resolution:
            print "Resolution of the DTM and map do not match"
            raise ValueError

        DTM.setNull(-9999)
        newRaster = np.copy(self.raster)

        for x in range(len(self.voxel_grid)):
            for y in range(len(self.voxel_grid[x])):
                for z in range(len(self.voxel_grid[x][y])):
                    #Several if statements used for optimization

                    #Ignores area with no data
                    if (DTM.voxel_grid[x][y] == -9999):
                        newRaster[x][y][z] = []
                        
                    #Any boxes below the ground are ignored entirely, whereas those completely above the ground are left alone
                    elif ((self.minZ + z + float(self.resolution) / 2) - DTM.voxel_grid[x][y] < height):
                        newRaster[x][y][z] = []

                    #If the block is on the edge, it looks at indivigual points
                    elif (((z - float(self.resolution) / 2) - DTM.voxel_grid[x][y]) < height):
                        #wait until the end of the check to delete to preserve the indexing location
                        toDel = []
                        for i in range(len(self.voxel_grid[x][y][z])):

                            if (self.pointCloud[self.raster[x][y][z][i]][2] - DTM.raster[x][y]) < height:
                                toDel.append(i)
                        #Clears out the points
                        newRaster[x][y][z] = np.delete(newRaster[x][y][z], toDel)

        filteredCR = CloudRasterizer(
            self.cloud_array, newRaster, self.resolution, DTM.xCor, DTM.yCor)
        return filteredCR

    def getGridSubsetXY(self, xMax, yMax, xMin, yMin):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        return np.copy(self.voxel_grid[xMin:xMax, yMin:yMax, :])

    def getXYPlane(self, layer):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        """
        Returns the raster array of the points in a specific layer
        """
        return np.copy(self.voxel_grid[:, :, layer])

    def getXZPlane(self, layer):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        return np.copy(self.voxel_grid[:, layer, :])

    def maxElevationRaster(self, layer=None):
        """
        Creates two dimensional array of maximum heights

        :param one two:

        :return:
        :rtype:
        """
        #gets the layer or flattens the whole map
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        tempMax = -9999
        for x in range(len(image)):
            for y in range(len(image[x])):
                if image[x][y] != []:
                    for i in range(len(image[x][y])):
                        if (self.pointCloud[image[x][y][i]][2] >= tempMax):
                            tempMax = self.pointCloud[image[x][y][i]][2]
                image[x][y] = tempMax
                tempMax = -9999

        return image

    def medianElevationRaster(self, layer=None):
        """
        Finds the median elevation

        :param int? layer: vertical index from ground?

        :return: elev_median_raster
        :rtype: raster

        If there is an even number of values, gets the average
        of the 2 middle values
        """
        #gets the layer or flattens the raster
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        for x in range(len(image)):
            for y in range(len(image[x])):
                if image[x][y] != []:
                    if len(image[x][y]) > 1:
                        heights = []

                        #Loops thorough all the points in the list
                        for i in image[x][y]:
                            heights.append(self.pointCloud[i][2])

                        image[x][y] = np.median(heights)
                    else:
                        image[x][y] = self.pointCloud[image[x][y][0]][2]
                else:
                    image[x][y] = -9999

        return image

    def colorRaster(self, layer=None):
        """
        Gives the color of the area

        :param one two:

        :return:
        :rtype:

        image = []
        if not layer:
            array = self.get2D()
            image = np.ndarray(shape=(len(array), len(array[0]), 3), dtype=np.float32)
        else:
            array = self.getHoriz(layer)
            image = np.ndarray(shape=(len(array), len(array[0]), 3), dtype=np.float32)

        tempMax = NO_DATA
        for x in range(len(array)):
            for y in range(len(array[x])):
                image[x][y] = [0, 0, 0]

                for i in range(len(array[x][y])):
                    if (self.pointCloud[array[x][y][i]][2] >= tempMax):
                        tempMax = self.pointCloud[array[x][y][i]][2]
                        red = NO_DATA
                        green = NO_DATA
                        blue = NO_DATA

                        if self.pointCloud[array[x][y][i]][3] >= 0:
                            red = self.pointCloud[array[x][y][i]][3]
                        else:
                            red = self.pointCloud[array[x][y][i]][3] + COLOR_ADJUST

                        if self.pointCloud[array[x][y][i]][4] >= 0:
                            green = self.pointCloud[array[x][y][i]][4]
                        else:
                            green = self.pointCloud[array[x][y][i]][4] + COLOR_ADJUST

                        if self.pointCloud[array[x][y][i]][5] >= 0:
                            blue = self.pointCloud[array[x][y][i]][5]
                        else:
                            blue = self.pointCloud[array[x][y][i]][5] + COLOR_ADJUST

                        image[x][y] =  [red/255,  green/255,  blue/255]

                tempMax = NO_DATA

        return image
        """
        pass


    #Gives the point density of the area
    #Layer is the layer to be scanned, -1 is all
    def pointDensityRaster(self, layer=None):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        #gets the layer or flattens the whole map
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        #Puts the number of pont in each xy location
        for x in range(len(image)):
            for y in range(len(image[x])):
                image[x][y] = len(image[x][y])

        return image

    #Height is an integer
    #DTM is the processed DTM file
    #Gets a raster of the maximum height in an area
    def maxHeightRaster(self, DTM, layer=None):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        overLap = self.mapOverlap(DTM)
        image = overLap.maxElevationRaster(layer)

        # get the differences bewteen the max elevation and the 
        # ground level to get the max height
        for x in range(len(image)):
            for y in range(len(image[x])):
                if image[x][y] == -9999:
                    image[x][y] = -9999
                else:
                    image[x][y] = image[x][y] - DTM.raster[x][y]

        finalX = DTM.xCor
        finalY = DTM.yCor
        finalCols = DTM.cols
        finalRows = DTM.rows

        if (self.minX < DTM.xCor):
            finalX = self.minX

        if (self.minY < DTM.yCor + DTM.rows):
            finalY = self.minX

        if (len(self.voxel_grid) < DTM.cols):
            finalCols = len(self.voxel_grid)

        if (len(self.voxel_grid[0]) < DTM.rows):
            finalRows = len(self.voxel_grid[0])

        return ahand.workableASCII(finalCols, finalRows, finalX, finalY, DTM.resolution, NO_DATA, image)

    #Height is an integer
    #DTM is the processed DTM file
    #Gets a raster of the median elevation of the area.
    def medianHeightRaster(self, layer, DTM):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        overLap = self.mapOverlap(DTM)
        image = overLap.medianElevationRaster(layer)

        #get the differences bewteen the median elevation and the ground level to get the median height
        for x in range(len(image)):
            for y in range(len(image[x])):
                if image[x][y] == NO_DATA:
                    image[x][y] = NO_DATA
                else:
                    image[x][y] = image[x][y] - DTM.raster[x][y]

        finalX = DTM.xCor
        finalY = DTM.yCor
        finalCols = DTM.cols
        finalRows = DTM.rows

        if (self.minX < DTM.xCor):
            finalX = self.minX

        if (self.minY < DTM.yCor + DTM.rows):
            finalY = self.minX

        if (len(self.voxel_grid) < DTM.cols):
            finalCols = len(self.voxel_grid)

        if (len(self.voxel_grid[0]) < DTM.rows):
            finalRows = len(self.voxel_grid[0])

        return ahand.workableASCII(finalCols, finalRows, finalX, finalY, DTM.resolution, NO_DATA, image)

    #Gives the ranges of all the heights for the slice
    def heightRangeRaster(self, layer=None):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        for x in range(len(image)):
            for y in range(len(image[x])):
                #Arbitrary starting values
                minVal = 9999
                maxVal = -9999

                #Get the min and max from unsorted values
                for index in image[x][y]:
                    if (self.pointCloud[index][2] < minVal):
                        minVal = self.pointCloud[index][2]
                    if (self.pointCloud[index][2] > maxVal):
                        maxVal = self.pointCloud[index][2]

                if (maxVal > minVal):
                    image[x][y] = maxVal - minVal
                else:
                    image[x][y] = -9999

        return image

    #Gives the standard Deviation of all the hights in the slice
    def standardDeviationRaster(self, layer=None):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        for x in range(len(image)):
            for y in range(len(image[x])):

                if (len(image[x][y]) > 0):
                    mean = 0
                    for index in image[x][y]:
                        mean += self.pointCloud[index][2]
                    mean = mean / len(image[x][y])

                    stdDev = 0
                    for index in image[x][y]:
                        stdDev += (self.pointCloud[index][2] - mean)**2
                    stdDev = stdDev / len(image[x][y])
                    stdDev = np.sqrt(stdDev)

                    image[x][y] = stdDev
                else:
                    image[x][y] = NO_DATA

        return image

    def Q95ElevationRaster(self, layer=None):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        for x in range(len(image)):
            for y in range(len(image[x])):
                if image[x][y] != []:
                    if len(image[x][y]) > 1:
                        heights = []

                        #Loops thorough all the points in the list
                        for i in image[x][y]:
                            heights.append(self.pointCloud[i][2]) 

                        image[x][y] = stats.scoreatpercentile(heights,95)
                    else:
                        image[x][y] = self.pointCloud[image[x][y][0]][2]
                else:
                    image[x][y] = NO_DATA

        return image

    def meanElevationRaster(self, layer=None):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        for x in range(len(image)):
            for y in range(len(image[x])):

                if (len(image[x][y]) > 0):
                    allEle = 0
                    for index in image[x][y]:
                        allEle += self.pointCloud[index][2]
                    image[x][y] = allEle / len(image[x][y])
                else:
                    image[x][y] = NO_DATA

        return image

    def meanHeightRaster(self, layer, DTM):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        image = self.mapOverlap(DTM).meanElevationRaster(layer)

        #get the differences bewteen the mean elevation and the ground level to get the mean height
        for x in range(len(image)):
            for y in range(len(image[x])):
                if image[x][y] == NO_DATA:
                    image[x][y] = NO_DATA
                else:
                    image[x][y] = image[x][y] - DTM.raster[x][y]

        finalX = DTM.xCor
        finalY = DTM.yCor
        finalCols = DTM.cols
        finalRows = DTM.rows

        if (self.minX < DTM.xCor):
            finalX = self.minX

        if (self.minY < DTM.yCor + DTM.rows):
            finalY = self.minX

        if (len(self.voxel_grid) < DTM.cols):
            finalCols = len(self.voxel_grid)

        if (len(self.voxel_grid[0]) < DTM.rows):
            finalRows = len(self.voxel_grid[0])

        return ahand.workableASCII(finalCols, finalRows, finalX, finalY, DTM.resolution, NO_DATA, image)

    def CVElevationRaster(self, layer=None):
        """
        Gives the CV raster of all the heights in a slice. The CV is the
        stanard deviation / mean for the slice

        :param one two:

        :return:
        :rtype:
        """
        image = []
        if not layer:
            image = self.get2D()
        else:
            image = self.getHoriz(layer)

        for x in range(len(image)):
            for y in range(len(image[x])):
                if (len(image[x][y]) > 0):
                    mean = 0
                    for index in image[x][y]:
                        mean += self.pointCloud[index][2]
                    mean = mean / len(image[x][y])

                    stdDev = 0
                    for index in image[x][y]:
                        stdDev += (self.pointCloud[index][2] - mean)**2
                    stdDev = stdDev / len(image[x][y])
                    stdDev = np.sqrt(stdDev)

                    image[x][y] = stdDev / mean
                else:
                    image[x][y] = NO_DATA

        return image

    def CVHeightRaster(self, dtm_cloud_voxel, layer=None):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        image = []
        if not layer:
            image = self.mapOverlap(dtm_cloud_voxel).get2D()
        else:
            image = self.mapOverlap(dtm_cloud_voxel).getHoriz(layer)

        for x in range(len(image)):
            for y in range(len(image[x])):
                if (len(image[x][y]) > 0):
                    mean = 0
                    for index in image[x][y]:
                        mean += self.pointCloud[index][2]
                    mean = mean / len(image[x][y])
                    mean -= dtm_cloud_voxel.raster[x][y]

                    stdDev = 0
                    for index in image[x][y]:
                        stdDev += (self.pointCloud[index][2] - mean)**2
                    stdDev = stdDev / len(image[x][y])
                    stdDev = np.sqrt(stdDev)

                    image[x][y] = stdDev / mean
                else:
                    image[x][y] = NO_DATA

        finalX = dtm_cloud_voxel.xCor
        finalY = dtm_cloud_voxel.yCor
        finalCols = dtm_cloud_voxel.cols
        finalRows = dtm_cloud_voxel.rows

        if (self.minX < dtm_cloud_voxel.xCor):
            finalX = self.minX

        if (self.minY < DTM.yCor + dtm_cloud_voxel.rows):
            finalY = self.minX

        if (len(self.voxel_grid) < dtm_cloud_voxel.cols):
            finalCols = len(self.voxel_grid)

        if (len(self.voxel_grid[0]) < dtm_cloud_voxel.rows):
            finalRows = len(self.voxel_grid[0])

        return ahand.workableASCII(finalCols, finalRows, finalX, finalY, DTM.resolution, NO_DATA, image)

    #returns a raster of the logarithmic density of the map
    def logDensityRaster(self):
        """
        (Unimplemented)
        about

        :param one two:

        :return:
        :rtype:
        """
        image = list(range(np.shape(self.voxel_grid)[1]) for x in range(np.shape(self.voxel_grid)[0]))

        #Sets the initial value of the image raster to 0
        for x in range(len(image)):
            for y in range(len(image[x])):
                image[x][y] = 0

        #Generates and sums the point density Raster for each layer
        for z in range(len(self.voxel_grid[0][0])):

            layerDensity = self.pointDensityRaster(z)

            for x in range(len(layerDensity)):
                for y in range(len(layerDensity[x])):
                    if (layerDensity[x][y] != 0):
                        image[x][y] = image[x][y] + (layerDensity[x][y] * math.log(layerDensity[x][y]))

        return image

    def mapOverlap(self):
        """
        about

        :param one two:

        :return:
        :rtype:
        """
        pass

#Comaprison methods

    #Lidar and DTM are processed into ASCII objects
    def compareClouds(self, compare_cloud_voxel, dtm_cloud_voxel):
        """
        about

        :param one two:

        :return:
        :rtype:
        """

        if (compare_cloud_voxel.resolution != self.resolution):
            print "Resolution of the lidar and map do not match"
            raise ValueError

        #gets the overlap bewteen the map and the DTM
        overMap = self.mapOverlap(dtm_cloud_voxel)
        overlap = overMap.maxHeightRaster(dtm_cloud_voxel)
        Xlen = 0
        Ylen = 0

        #gets the overlap bewteen the map-generated raster and the lidar
        if (len(overlap.raster) > len(compare_cloud_voxel.raster)):
            Xlen = len(compare_cloud_voxel.raster)
        else:
            Xlen = len(overlap.raster)

        if (len(overlap.raster[0]) > len(compare_cloud_voxel.raster[0])):
            Ylen = len(compare_cloud_voxel.raster[0])
        else:
            Ylen = len(overlap.raster)

        print
        print Xlen
        print Ylen
        print

        #Does the comparison bewteen the map data and the lidar data
        overlap.raster = np.subtract(overlap.raster[:Xlen, :Ylen], compare_cloud_voxel.raster[:Xlen, :Ylen])

        #Goes through and grabs values that were NO_DATA compared to actual data points
        #only works if all given data points are positive, works here beacuse height is always positive
        for x in range(len(overlap.raster)):
            for y in range(len(overlap.raster[x])):
                if ((overlap[x][y] < NO_DATA) or (overlap[x][y] > abs(NO_DATA))):
                    overlap[x][y] = NO_DATA

        return overlap
