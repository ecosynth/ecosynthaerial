"""
Postprocess
===========
"""
import StringIO

import numpy as np

from . import io
from . import transforms
from . import CloudVoxelizer
from ecosynth.acquire.utilities import telemetry_to_gps_positions

__all__ = ['run']

def run(out_file, log_file, msl_float, ply_file=None, gps_file=None, logger_file=None, dense_ply_file_in=None, dense_ply_file_out=None):
    """
    Entry point for Post-Processing stage of automated pipeline

    :param file out_file: Bundler OUT file
    :param file log_file: LOG file from Acquisition stage
    :param float msl_float: lauch altitude (meters above mean sea level)
    :param file ply_file: Clean file to write georeferenced and
        noise-filtered PLY
    :param file gps_file: (Optional) Clean file to write GPS positions
    :param file logger_file: (Optional) File currently for debugging and point cloud statistics
    :param file dense_ply_file_in: (Optional) Dense PLY File to Transform
    :param file dense_ply_file_out: (Optional) Transformed Dense PLY File

    :return: cv_object
    :rtype: CloudVoxelizer

    | **Notes**
    | here

    | **Example**
    | here

    """

    #  Setup
    if gps_file:
        gps_file_buf = gps_file
    else:
        gps_file_buf = StringIO.StringIO()

    gps_array = telemetry_to_gps_positions(
        log_file, msl_float, gps_file=gps_file_buf)

    #  Input Data Conversion
    out_tuple = io.load_out(out_file)  # Load OUT File

    #  Georeference Primary Points
    xyzrgb_array_georef, params = transforms.georef_telemetry(
        out_tuple, gps_array, msl_float, logger=logger_file)

    # Georeference and Save Dense Points
    if (dense_ply_file_in and dense_ply_file_out):
        ply_tuple = io.load_ply(dense_ply_file_in)
        dense_georef_array = transforms.apply_helmert(
            params, ply_tuple.xyzrgb_array)
        io.save_ply(dense_ply_file_out, dense_georef_array)

    #  Filter Noise from Primary
    cv_object = CloudVoxelizer.CloudVoxelizer(
        xyzrgb_array_georef, resolution=1)
    cv_object.filter_noise_z()

    #  Save Primary as PLY
    if ply_file:
        xyzrgb_array_geo_noise = cv_object.get_cloud_array()
        io.save_ply(ply_file, xyzrgb_array_geo_noise)

    #  EcoBrowser

    return cv_object
