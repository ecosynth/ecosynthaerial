from bottle import request, route, run


@route('/upload', method='GET')
def send_form():
    return '''<form action="/upload" method="post" enctype="multipart/form-data">
  Select a file: <input type="file" name="upload" />
  <input type="submit" value="Start upload" />
</form>'''


@route('/upload', method='POST')
def do_upload():
    upload = request.files.get('upload')

    print upload.file

    return 'OK'

run(host='localhost', port=8081, debug=True)
