import os
import webbrowser

from bottle import route
from bottle import run
from bottle import template
from bottle import static_file
from bottle import redirect
from bottle import request

from bottle import SimpleTemplate

from ecosynth import postprocess as pp

directory = os.path.dirname(__file__)


@route('/hello/<name>')
def example(name):
    return template('<b>You have reached the {{name}} page</b>!', name=name)


@route('/')
def toIndex():
    redirect('/Ecosynth/')


@route('/Ecosynth/')
def index():
    f = open(
        os.path.join(directory, "static/html/index.html"))
    return f


@route('/Ecosynth/Acquisition')
def acquisition():
    pass


@route('/Ecosynth/Generation')
def generation():
    pass


@route('/Ecosynth/PostProcessing', method="GET")
def send_postprocessing_form():
    f = open(
        os.path.join(directory, "static/html/postprocess_form.html"))

    return f


@route('/Ecosynth/PostProcessing', method="POST")
def run_postprocessing():
    #try:
        # Fetch Inputs
        out_file = request.files.get('out_file')
        log_file = request.files.get('log_file')
        msl_float = float(request.forms.get('msl_float'))

        '''
        output_dir = request.forms.get('output_dir')

        # Data Processing
        if output_dir:
            #ply_file = open(output_dir + 'ply_file.ply', 'w')
            #gps_file = open(output_dir + 'gps_file.txt', 'w')

            cv_object = pp.postprocess.run(
                out_file, log_file, msl_float)
                #ply_file=ply_file, gps_file=gps_file)

        else:
        '''
        cv_object = pp.postprocess.run(out_file.file, log_file.file, msl_float)

        color_raster = cv_object.get_color_raster()
        q95_raster = cv_object.get_Q95_elevation_raster()
        point_density_raster = cv_object.get_point_density_raster()

        cv_object.plot_raster_heatmap(q95_raster)
        # Save Outputs
        #f_color = open(output_dir + "color.ply", )

        return "Processing Complete"

    #except:
        #raise Exception
        #return "Processing Failure"

@route('/Ecosynth/Sharing')
def sharing():
    f = open("form.html")
    return f


@route('/Ecosynth/static/<filepath:path>')
def server_static(filepath):
    root = os.path.join(directory, "static")
    return static_file(filepath, root=root)


def launch():
    url = "http://localhost:8081/Ecosynth/"
    webbrowser.open(url)

    run(host='localhost', port=8081, debug=True)


if __name__ == "__main__":
    run(host='localhost', port=8081, debug=True)
