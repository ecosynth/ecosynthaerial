#Ecosynth Library

Documentation is located at [Not Available](http://code.ecosynth.org)

##Requirements

This library uses several third party libraries listed in requirements.txt

The simplest way to make sure you have them installed is to change into the source directory using a terminal and issue the following command

make deps
